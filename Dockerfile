FROM node:16-alpine3.18 as builder

WORKDIR /usr/src/app
COPY package*.json ./

RUN npm install


COPY src/ ./src/
COPY public/ ./public/
RUN npm run build

FROM nginx:1.19-alpine
COPY --from=builder /usr/src/app/build /usr/share/nginx/html
COPY default.conf /etc/nginx/conf.d/default.conf

EXPOSE 80

CMD ["nginx", "-g", "daemon off;"]