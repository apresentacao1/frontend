import logo from './logo.svg';
import {useState} from "react";
import axios from "axios";

function App() {
  const [palavra, setPalavra] = useState('')
  const [resposta, setResposta] = useState('')


  function gerarHash(e) {
    e.preventDefault()
    const entrada = e.target.value
    setPalavra(entrada)
    if (!entrada) {
      setResposta('')
      return
    }
    axios.post(process.env.REACT_APP_SERVER+'/hash', {palavra})
      .then(res => setResposta(res.data))
  }

  return (
    <div className="container">
        <div className="row mt-5">
            <div className="text-center">
                <h1>MEU HASH SHA256</h1>
            </div>
        </div>
        <div className="row">
            <div className="col-12">
                <input
                    value={palavra}
                    onChange={gerarHash}
                    className="col-5 form-control"
                    placeholder="Entre com a palavra"/>
            </div>
          <div className="col-12 mt-3 text-center">
             Hash: <b>{resposta ? resposta : 'Sem conteudo' }</b>
          </div>
        </div>

    </div>
  );
}

export default App;
